﻿namespace DistanceCalculator.Models
{
    public class Airport
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string timezone_region_name { get; set; }
        public string City_IATA { get; set; }
        public string IATA { get; set; }
        public string Country_IATA { get; set; }
        public int Rating { get; set; }
        public Position Location { get; set; }
        public string Type { get; set; }
        public int Hubs { get; set; }
    }
}
