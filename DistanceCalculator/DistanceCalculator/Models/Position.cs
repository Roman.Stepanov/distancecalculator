﻿namespace DistanceCalculator.Models
{
    public class Position
    {
        public double Lon { get; set; }
        public double Lat { get; set; }
    }
}
