﻿using DistanceCalculator.Services;
using DistanceCalculator.ViewModels;
using Xamarin.Forms;

namespace DistanceCalculator.Pages
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = CustomRouting.GetOrCreateViewModel(this.GetType());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            (BindingContext as MainViewModel)?.Init();
        }
    }
}
