﻿using DistanceCalculator.Pages;
using DistanceCalculator.Services;
using DistanceCalculator.ViewModels;
using Refit;
using SimpleInjector;

namespace DistanceCalculator
{
    public class ViewResolver
    {
        public static Container Container { get; protected set; }

        public virtual void CreateContainer()
        {
            Container = new Container();

            Container.Options.EnableAutoVerification = false;
            Container.Register<App>(Lifestyle.Singleton);
            Container.Register<IDialogService, DialogService>(Lifestyle.Singleton);
            Container.Register<IHaversine, Haversine>(Lifestyle.Singleton);
            Container.Register<INavigationService, NavigationService>(Lifestyle.Singleton);

            var refitSettings = new RefitSettings { CollectionFormat = CollectionFormat.Multi };
            var airportService = RestService.For<IAirportService>(Constants.URL, refitSettings);
            Container.RegisterInstance(typeof(IAirportService), airportService);

            AddPages();
            NavigationService.RegisterRoutes();
        }

        private void AddPages()
        {
            Container.Register<MainPage>();
            Container.Register<MainViewModel>();
        }

        public static T Resolve<T>() where T : class => Container.GetInstance<T>();
    }
}
