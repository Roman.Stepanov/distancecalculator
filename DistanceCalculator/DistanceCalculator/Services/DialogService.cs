﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace DistanceCalculator.Services
{
    public class DialogService : IDialogService
    {
        public async Task ShowAlert(string title, string message)
        {
            var currentPage = Application.Current.MainPage;
            if (currentPage == null)
                return;

            if (currentPage is ContentPage page)
                await page.DisplayAlert(title, message, "OK");
        }
    }
}
