﻿using DistanceCalculator.Models;

namespace DistanceCalculator.Services
{
    public interface IHaversine
    {
        double Distance(Position pos1, Position pos2);
    }
}
