﻿using DistanceCalculator.Models;
using System;

namespace DistanceCalculator.Services
{
    public class Haversine : IHaversine
    {
        public double Distance(Position pos1, Position pos2)
        {
            double R = 3960;

            double dLat = ToRadian(pos2.Lat - pos1.Lat);
            double dLon = ToRadian(pos2.Lon - pos1.Lon);

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                Math.Cos(ToRadian(pos1.Lat)) * Math.Cos(ToRadian(pos2.Lat)) *
                Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            double c = 2 * Math.Asin(Math.Min(1, Math.Sqrt(a)));
            double d = R * c;

            return d;
        }

        public double ToRadian(double val)
        {
            return (Math.PI / 180) * val;
        }
    }
}
