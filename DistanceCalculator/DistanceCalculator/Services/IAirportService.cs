﻿using DistanceCalculator.Models;
using Refit;
using System.Threading.Tasks;

namespace DistanceCalculator.Services
{
    public interface IAirportService
    {
        [Get("/airports/{IATA}")]
        Task<Airport> GetAirport(string IATA);
    }
}
