﻿using DistanceCalculator.Pages;
using DistanceCalculator.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace DistanceCalculator.Services
{
    public class NavigationService : INavigationService
    {
        public static void RegisterRoutes()
        {
            CustomRouting.RegisterRoute(typeof(MainViewModel), typeof(MainPage));
        }
    }

    public static class CustomRouting
    {
        private static IDictionary<Type, Type> _dictionary = new Dictionary<Type, Type>();

        public static Page GetPage(Type vmType)
        {
            var pageType = _dictionary[vmType];
            var page = (Page)Activator.CreateInstance(pageType);
            return page;
        }

        public static Page GetPageWithBinding(Type vmType, object data)
        {
            var pageType = _dictionary[vmType];
            var page = (Page)ViewResolver.Container.GetInstance(pageType);

            return page;
        }


        public static object GetOrCreateViewModel(Type pageType)
        {
            var vmType = _dictionary.FirstOrDefault(x => x.Value == pageType).Key;
            if (vmType == null)
                return null;
            var vm = ViewResolver.Container.GetInstance(vmType);
            return vm;
        }

        public static void RegisterRoute(Type vmType, Type pageType)
        {
            if (!_dictionary.ContainsKey(vmType))
                _dictionary.Add(vmType, pageType);
            else
                _dictionary[vmType] = pageType;
        }

        public static void UnRegisterRoutes() => _dictionary.Clear();
    }
}
