﻿using System.Threading.Tasks;

namespace DistanceCalculator.Services
{
    public interface IDialogService
    {
        Task ShowAlert(string title, string message);
    }
}
