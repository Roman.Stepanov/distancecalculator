﻿using DistanceCalculator.Models;
using DistanceCalculator.Resources;
using DistanceCalculator.Services;
using Refit;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace DistanceCalculator.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IAirportService _airportService;
        private readonly IDialogService _dialogService;
        private readonly IHaversine _haversine;

        public ICommand CalculateDistanceCommand { get; }

        internal void Init()
        {
            IsValidDepartureAirport = false;
            IsValidArrivalAirport = false;
        }

        private string _result;
        public string Result
        {
            get { return _result; }
            set { SetProperty(ref _result, value); }
        }

        private string _departureAirport;
        public string DepartureAirport
        {
            get { return _departureAirport; }
            set { SetProperty(ref _departureAirport, value); }
        }

        private string _arrivalAirport;
        public string ArrivalAirport
        {
            get { return _arrivalAirport; }
            set { SetProperty(ref _arrivalAirport, value); }
        }

        private bool _isValidDepartureAirport = false;
        public bool IsValidDepartureAirport
        {
            get { return _isValidDepartureAirport; }
            set { SetProperty(ref _isValidDepartureAirport, value); }
        }


        private bool _isValidArrivalAirport = false;
        public bool IsValidArrivalAirport
        {
            get { return _isValidArrivalAirport; }
            set { SetProperty(ref _isValidArrivalAirport, value); }
        }

        public MainViewModel(IDialogService dialogService, IHaversine haversine, IAirportService airportService)
        {
            _dialogService = dialogService;
            _haversine = haversine;
            _airportService = airportService;

            CalculateDistanceCommand = new Command(CalculateDistance, () => IsValidDepartureAirport && IsValidArrivalAirport && !IsBusy);
        }

        private async void CalculateDistance()
        {
            if (!CalculateDistanceCommand.CanExecute(null))
            {
                if (!IsValidDepartureAirport && !IsValidArrivalAirport)
                {
                    await _dialogService.ShowAlert("", Localization.WrongIATA);
                    return;
                }

                if (!IsValidDepartureAirport)
                {
                    await _dialogService.ShowAlert("", Localization.WrongDepartureIATA);
                    return;
                }

                if (!IsValidArrivalAirport)
                {
                    await _dialogService.ShowAlert("", Localization.WrongArrivalIATA);
                    return;
                }

                return;
            }

            IsBusy = true;
            
            try
            {
                var getAirport1 = _airportService.GetAirport(DepartureAirport.ToUpper());
                var getAirport2 = _airportService.GetAirport(ArrivalAirport.ToUpper());
                await Task.WhenAll(getAirport1, getAirport2);
                Airport airport1 = getAirport1.Result;
                Airport airport2 = getAirport2.Result;

                var distance = _haversine.Distance(airport1.Location, airport2.Location);
                Result = airport1.City + " - " + airport2.City + " " + distance.ToString("N") + " NM";
            }
            catch (ApiException e)
            {
                if (e.StatusCode == HttpStatusCode.NotFound)
                {
                    string iata = e.Uri.LocalPath.Substring(e.Uri.LocalPath.Length - 3);
                    await _dialogService.ShowAlert(Localization.Error, string.Format(Localization.NotFoundIATA, iata));
                }
                else
                {
                    await _dialogService.ShowAlert(Localization.Error, e.Message);
                }
            }
            catch(Exception e)
            {
                await _dialogService.ShowAlert(Localization.Error, e.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
